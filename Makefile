run:
	@python manage.py runserver

test:
	@python manage.py test

createsuperuser:
	@python manage.py createsuperuser

migrate:
	@python manage.py migrate

shell:
	@python manage.py shell

