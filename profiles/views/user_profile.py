from django.contrib.auth.models import User

from rest_framework import viewsets, permissions
from rest_framework.decorators import permission_classes
from rest_framework.response import Response

from profiles.models import Department
from profiles.serializers import UserSerializer, UserProfileDetailSerializer


class UserProfileViewSet(viewsets.ViewSet):
    def get_queryset(self):
        return User.objects.all()

    # Helper methods
    # --------------
    def get_first_name(self, full_name):
        return full_name.split(" ")[0]

    def get_last_name(self, full_name):
        return full_name.split(" ")[1] if len(full_name.split(" ")) > 1 else ""

    def get_department(self, name):
        if Department.objects.filter(name=name).exists():
            return Department.objects.get(name=name)

        return None

    def update_user(self, user, data):
        email = data.get('email')
        full_name = data.get('full_name')
        department = data.get('department')

        first_name = self.get_first_name(full_name)
        last_name = self.get_last_name(full_name)
        department = self.get_department(department)

        if not department:
            return False, "Department does not exist!"

        try:
            user.email = email
            user.first_name = first_name
            user.last_name = last_name
            user.profile.department = department
            user.save()

            return True, ""
        except Exception as e:
            return False, "Could not update user! Error: {}".format(str(e))

    def delete_user_profile(self, user):
        user.email = ""
        user.first_name = ""
        user.last_name = ""
        user.profile.department = None
        user.save()

    # Viewset methods
    # ---------------
    @permission_classes(permissions.IsAuthenticated, )
    def retrieve(self, request, pk):
        queryset = self.get_queryset().filter(profile__id=pk)
        serializer = UserProfileDetailSerializer(queryset, many=True)

        return Response(serializer.data)

    @permission_classes(permissions.IsAuthenticated, )
    def list(self, request):
        queryset = self.get_queryset()
        serializer = UserSerializer(queryset, many=True)

        return Response(serializer.data)

    @permission_classes(permissions.IsAuthenticated, )
    def update(self, request, pk):
        user = self.get_queryset().filter(profile__id=pk).first()
        if not user:
            message = "User profile with id {} does not exist!".format(pk)
            return Response({"error": message}, status=401)

        if user.profile.department != request.user.profile.department:
            if not request.user.is_staff:
                message = "Not allowed to update user profile with id {}!".format(pk)
                return Response({"error": message}, status=401)

        status, message = self.update_user(user, request.data)
        if not status:
            return Response({"error": message}, status=400)

        return Response({"message": "User profile updated!"})

    @permission_classes(permissions.IsAuthenticated, )
    def destroy(self, request, pk):
        user = request.user
        if user.is_staff:
            user = User.objects.filter(profile__id=pk).first()
        elif user.profile.id != pk:
            message = "Not allowed to delete user profile with id {}!".format(pk)
            return Response({"error": message}, status=401)

        self.delete_user_profile(user)
        return Response({"message": "User profile deleted!"})
