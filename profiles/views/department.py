from rest_framework import viewsets, permissions
from rest_framework.decorators import permission_classes
from rest_framework.response import Response

from profiles.models import Department
from profiles.serializers import DepartmentSerializer


class DepartmentViewSet(viewsets.ViewSet):
    def get_queryset(self):
        return Department.objects.all()

    @permission_classes((permissions.IsAuthenticated, ))
    def list(self, request):
        queryset = self.get_queryset()
        serializer = DepartmentSerializer(queryset, many=True)

        return Response(serializer.data)

    @permission_classes((permissions.IsAuthenticated, ))
    def retrieve(self, request, pk):
        queryset = self.get_queryset().filter(id=pk)
        serializer = DepartmentSerializer(queryset, many=True)

        return Response(serializer.data)

    @permission_classes(permissions.IsAuthenticated, )
    def create(self, request):
        if not request.user.is_staff:
            return Response({"error": "Not allowed!"}, status=403)

        department_name = request.data.get('name')
        Department.objects.get_or_create(name=department_name)

        queryset = self.get_queryset()
        serializer = DepartmentSerializer(queryset, many=True)

        return Response(serializer.data)

    @permission_classes(permissions.IsAuthenticated, )
    def destroy(self, request, pk):
        if not request.user.is_staff:
            return Response({"error": "Not allowed!"}, status=403)

        try:
            department = Department.objects.get(id=pk)
            department.delete()
        except Department.DoesNotExist:
            error_msg = "Department with id {0} does not exist!".format(pk)
            return Response({"error": error_msg}, status=400)

        queryset = self.get_queryset()
        serializer = DepartmentSerializer(queryset, many=True)

        return Response(serializer.data)
