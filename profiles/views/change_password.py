from django.contrib.auth.hashers import check_password

from rest_framework import viewsets, permissions
from rest_framework.response import Response


class ChangePasswordViewSet(viewsets.ViewSet):
    permission_classes = (permissions.IsAuthenticated, )

    def create(self, request):
        old_password = request.data.get('old_password')
        new_password = request.data.get('new_password')

        if check_password(old_password, request.user.password):
            request.user.set_password(new_password)
            request.user.save()
            return Response({"message": "Password changed!"})

        return Response({"message": "Invalid password!"}, status=400)
