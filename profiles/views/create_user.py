from django.contrib.auth.models import User

from rest_framework import permissions, viewsets
from rest_framework.response import Response

from profiles.models import Department
from profiles.serializers import UserProfileDetailSerializer


class CreateUserViewSet(viewsets.ViewSet):
    permission_classes = (permissions.AllowAny, )

    def get_queryset(self):
        return User.objects.all()

    def get_first_name(self, full_name):
        return full_name.split(" ")[0]

    def get_last_name(self, full_name):
        return full_name.split(" ")[1] if len(full_name.split(" ")) > 1 else ""

    def get_department(self, name):
        if Department.objects.filter(name=name).exists():
            return Department.objects.get(name=name)

        return None

    def create_user(self, data):
        username = data.get('username')
        email = data.get('email')
        password = data.get('password')
        full_name = data.get('full_name')
        department = data.get('department')

        first_name = self.get_first_name(full_name)
        last_name = self.get_last_name(full_name)
        department = self.get_department(department)

        if not department:
            return False, "Department does not exist!"

        try:
            user = User.objects.create_user(username=username,
                                            password=password,
                                            email=email,
                                            first_name=first_name,
                                            last_name=last_name)

            user.profile.department = department
            user.save()

            return True, ""
        except Exception as e:
            return False, "Could not create user! Error: {}".format(str(e))

    def create(self, request):
        status, message = self.create_user(request.data)
        if not status:
            return Response({"error": message}, status=400)

        queryset = self.get_queryset().filter(username=request.data.get('username'))
        serializer = UserProfileDetailSerializer(queryset, many=True)

        return Response(serializer.data)