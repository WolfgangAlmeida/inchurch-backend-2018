from .change_password import ChangePasswordViewSet
from .department import DepartmentViewSet
from .user_profile import UserProfileViewSet
from .create_user import CreateUserViewSet