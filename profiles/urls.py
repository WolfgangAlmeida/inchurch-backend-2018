from rest_framework import routers

from profiles.views import (DepartmentViewSet,
                            UserProfileViewSet,
                            ChangePasswordViewSet,
                            CreateUserViewSet)

router = routers.DefaultRouter()
router.register('user-profile', UserProfileViewSet, base_name='user-profile')
router.register('create-user', CreateUserViewSet, base_name='create-user')
router.register('change-password', ChangePasswordViewSet, base_name='change-password')
router.register('department', DepartmentViewSet, base_name='department')
