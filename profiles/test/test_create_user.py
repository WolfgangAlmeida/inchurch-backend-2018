from django.test import TestCase
from django.contrib.auth.models import User

from rest_framework.test import APIClient

from profiles.models import Department


class UserProfileTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.department = Department.objects.create(name="TI")

    def test_create_user_api_success(self):
        request_data = {
            "username": "test2",
            "password": "test2",
            "email": "test2@test2.com",
            "full_name": "Test2 Testson",
            "department": "TI"
        }
        response = self.client.post(path="/api/create-user/", data=request_data)
        data = response.json()

        TestCase.assertTrue(self, len(data) == 1)
        TestCase.assertEquals(self, data[0]['username'], request_data['username'])
        TestCase.assertEquals(self, data[0]['department']['name'], request_data['department'])
        TestCase.assertTrue(self, User.objects.filter(username=data[0]['username']).exists())

    def test_create_user_api_fail(self):
        request_data = {
            "username": "invalid",
            "password": "invalid",
            "email": "invalid@invalid.com",
            "full_name": "Invalid Invalidson",
            "department": None
        }
        response = self.client.post(path="/api/create-user/", data=request_data)
        data = response.json()

        TestCase.assertIn(self, "error", data)
        TestCase.assertIn(self, "Department does not exist!", data['error'])

        request_data['department'] = "TI"
        self.client.post(path="/api/create-user/", data=request_data)

        response = self.client.post(path="/api/create-user/", data=request_data)
        data = response.json()

        TestCase.assertIn(self, "error", data)
        TestCase.assertIn(self, "Could not create user!", data['error'])
