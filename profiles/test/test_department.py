from django.test import TestCase
from django.contrib.auth.models import User

from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token

from profiles.models import Department


class DepartmentTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.departments = ["TI", "RH"]

        Department.objects.create(name=self.departments[0])
        Department.objects.create(name=self.departments[1])

        self.user = User.objects.create_superuser(username="test",
                                                  password="test",
                                                  email="test@test.com")
        self.token = Token.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

        self.user_alt = User.objects.create(username="test2",
                                            password="test2",
                                            email="test2@test.com")
        self.token_alt = Token.objects.create(user=self.user_alt)

    def test_department_api_list(self):
        response = self.client.get("/api/department/")
        data = response.json()

        TestCase.assertEquals(self, len(data) >= 2, True)
        TestCase.assertIn(self, data[0]['name'], self.departments)
        TestCase.assertIn(self, data[1]['name'], self.departments)

    def test_department_api_retrieve(self):
        response = self.client.get("/api/department/1/")
        data = response.json()

        TestCase.assertEquals(self, len(data) == 1, True)
        TestCase.assertIn(self, data[0]['name'], self.departments)

    def test_department_api_create_destroy_success(self):
        # Creating
        response = self.client.post(path="/api/department/", data={"name": "ADM"})
        data = response.json()

        TestCase.assertEquals(self, len(data) >= 3, True)

        new_department = next((item for item in data if item['name'] == "ADM"), None)
        TestCase.assertIsNotNone(self, new_department)

        # Destroying
        response = self.client.delete(path="/api/department/{}/".format(new_department['id']))
        data = response.json()

        new_department = next((item for item in data if item['name'] == "ADM"), None)
        TestCase.assertIsNone(self, new_department)

    def test_department_api_create_destroy_fail(self):
        # Trying to create
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token_alt.key)
        response = self.client.post(path="/api/department/", data={"name": "ADM"})
        data = response.json()

        TestCase.assertIn(self, 'error', data)
        TestCase.assertEquals(self, "Not allowed!", data['error'])

        # Creating properly
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(path="/api/department/", data={"name": "ADM"})
        data = response.json()

        new_department = next((item for item in data if item['name'] == "ADM"), None)

        # Trying to destroy
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token_alt.key)
        response = self.client.delete(path="/api/department/{}/".format(new_department['id']))
        data = response.json()

        TestCase.assertIn(self, 'error', data)
        TestCase.assertEquals(self, "Not allowed!", data['error'])

    def tearDown(self):
        self.user.delete()
        self.user_alt.delete()
