from django.test import TestCase
from django.contrib.auth.models import User

from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token

from profiles.models import Department


class UserProfileTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username="test",
                                             password="test",
                                             email="test@test.com",
                                             first_name="Test",
                                             last_name="Testson")
        self.department = Department.objects.create(name="TI")
        self.user.profile.department = self.department
        self.user.save()

        self.token = Token.objects.create(user=self.user)

        self.user_alt = User.objects.create_user(username="test_alt",
                                                 password="test_alt",
                                                 email="test_alt@testalt.com",
                                                 first_name="Testalt",
                                                 last_name="Testson")
        self.department = Department.objects.create(name="RH")
        self.user_alt.profile.department = self.department
        self.user_alt.save()

        self.token_alt = Token.objects.create(user=self.user_alt)

    def test_user_profile_api_list(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.get("/api/user-profile/")
        data = response.json()

        TestCase.assertTrue(self, len(data) >= 1)
        TestCase.assertIn(self, "full_name", data[0])
        TestCase.assertEquals(self, data[0]["full_name"], "Test Testson")

    def test_user_profile_api_retrieve(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.get("/api/user-profile/1/")
        data = response.json()

        TestCase.assertTrue(self, len(data) == 1)
        user_profile = data[0]

        TestCase.assertIn(self, "department", user_profile)
        TestCase.assertIsNotNone(self, user_profile['department'])
        TestCase.assertIn(self, user_profile['department']['name'], ["TI", "RH"])

    def test_user_profile_api_update_success(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        request_data = {
            "email": "test2@test2.com",
            "full_name": "Test2 Testson",
            "department": "TI"
        }

        response = self.client.put(path="/api/user-profile/{}/".format(self.user.profile.id),
                                   data=request_data)
        data = response.json()

        TestCase.assertIn(self, "message", data)
        TestCase.assertEquals(self, data['message'], "User profile updated!")

    def test_user_profile_api_update_fail_department(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        request_data = {
            "email": "test2@test2.com",
            "full_name": "Test2 Testson",
            "department": "INVALID"
        }

        response = self.client.put(path="/api/user-profile/{}/".format(self.user.profile.id),
                                   data=request_data)
        data = response.json()

        TestCase.assertIn(self, "error", data)
        TestCase.assertEquals(self, data['error'], "Department does not exist!")

    def test_user_profile_api_update_fail_user(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        request_data = {
            "email": "test2@test2.com",
            "full_name": "Test2 Testson",
            "department": "TI"
        }

        response = self.client.put(path="/api/user-profile/999999/", data=request_data)
        data = response.json()

        TestCase.assertIn(self, "error", data)
        TestCase.assertEquals(self, data['error'], "User profile with id 999999 does not exist!")

    def test_user_profile_api_update_fail_not_allowed(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token_alt.key)
        request_data = {
            "email": "test2@test2.com",
            "full_name": "Test2 Testson",
            "department": "TI"
        }

        response = self.client.put(path="/api/user-profile/{}/".format(self.user.profile.id),
                                   data=request_data)
        data = response.json()

        TestCase.assertIn(self, "error", data)
        TestCase.assertEquals(self,
                              data['error'],
                              "Not allowed to update user profile with id {}!".format(self.user.profile.id))


    def tearDown(self):
        self.department.delete()
        self.user.delete()
        self.user_alt.delete()
