from django.test import TestCase
from django.contrib.auth.models import User

from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token


class ChangePasswordTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.valid_credentials = {"username": "test", "password": "test"}

        self.user = User.objects.create_user(username="test",
                                             password="test",
                                             email="test@test.com")
        self.token = Token.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    def test_change_password_success(self):
        # Changing
        response = self.client.post(path="/api/change-password/",
                                    data={"old_password": "test", "new_password": "test123"})
        data = response.json()

        TestCase.assertIn(self, "changed!", data['message'])

        self.valid_credentials["password"] = "test123"

        # Returning
        response = self.client.post(path="/api/change-password/",
                                    data={"old_password": "test123", "new_password": "test"})
        data = response.json()

        TestCase.assertIn(self, "changed!", data['message'])

        self.valid_credentials["password"] = "test"

    def test_change_password_fail(self):
        response = self.client.post(path="/api/change-password/",
                                    data={"old_password": "invalid", "new_password": "invalid"})
        data = response.json()

        TestCase.assertIn(self, "Invalid", data['message'])

    def test_change_password_fail_no_auth(self):
        self.client.credentials()
        response = self.client.post(path="/api/change-password/",
                                    data={"old_password": "invalid", "new_password": "invalid"})
        data = response.json()

        TestCase.assertNotIn(self, "message", data)
        TestCase.assertIn(self, "detail", data)
        TestCase.assertIn(self, "Authentication credentials were not provided.", data['detail'])
