from django.contrib.auth.models import User

from rest_framework import serializers

from profiles.models import Department


# Create your serializers here
# ============================
class UserSerializer(serializers.HyperlinkedModelSerializer):
    full_name = serializers.SerializerMethodField()
    profile_id = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        return obj.get_full_name()

    def get_profile_id(self, obj):
        return obj.profile.id

    class Meta:
        model = User
        fields = ('profile_id', 'full_name', )


class UserProfileDetailSerializer(serializers.HyperlinkedModelSerializer):
    full_name = serializers.SerializerMethodField()
    profile_id = serializers.SerializerMethodField()
    department = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        return obj.get_full_name()

    def get_profile_id(self, obj):
        return obj.profile.id

    def get_department(self, obj):
        return {
            "id": obj.profile.department.id,
            "name": obj.profile.department.name
        } if obj.profile.department else None

    class Meta:
        model = User
        fields = ('profile_id', 'username', 'email', 'full_name', 'is_staff', 'department', )


class DepartmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Department
        fields = ('id', 'name', )
