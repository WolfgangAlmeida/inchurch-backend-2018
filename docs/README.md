# inChurch Recruitment Test

## Instalação:

1 - Crie uma virtualenv para o projeto:

```bash
$ mkdir venv
$ virtualenv venv/ --python=python3.6
$ source venv/bin/activate
```

2 - Instale as dependências do projeto:

```bash
(venv) $ pip install -r requirements.txt
``` 

3 - Rode as migrações e crie uma conta de superusuário:

```bash
(venv) $ make migrate
(venv) $ make createsuperuser
```

4 - Rode o projeto:

```bash
(venv) $ make run
```

Caso queira executar os testes, utilize o comando abaixo:

```bash
(venv) $ make test
```

## Autenticação:

Este projeto utiliza Token Authentication fornecido pelo Django Rest Framework.

Diversas endpoints neste projeto necessitam de Token para serem acessados.
Para gerar um Token válido para seu usuário, utilize o comando abaixo no terminal:

```bash
curl -X POST \
  http://localhost:8000/auth-token/ \
  -H 'Content-Type: application/json' \
  -d '{"username": "admin", "password": "admin"}'
```

E então você receberá o Token da seguinte forma:

```json
{
    "token": "7ee25910ac2842f0963aea046b44a314ec3e10a6"
}
```

Com este Token, será possível acessar os endpoints protegidos.
Aqui está um exemplo de como acessar um endpoint que requer autenticação:

```bash
curl -X GET \
  http://localhost:8000/api/department/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6'
```

## Endpoints:

#### Create User

Endereço: ```http://localhost:8000/api/create-user/```

Tipo: ```POST```

Exemplo:

```bash
curl -X POST \
  http://localhost:8000/api/create-user/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6' \
  -H 'Content-Type: application/json' \
  -d '{
	"username": "user1",
	"password": "passwd1",
	"email": "user1@test.com",
	"full_name": "Fulano Silva",
	"department": "TI"
}'
```

Retorno:

```json
[
    {
        "profile_id": 3,
        "username": "user1",
        "email": "user1@test.com",
        "full_name": "Fulano Silva",
        "is_staff": false,
        "department": {
            "id": 1,
            "name": "TI"
        }
    }
]
```

***

#### Change Password

Endereço: ```http://localhost:8000/api/change-password/```

Tipo: ```POST```

Exemplo:

```bash
curl -X POST \
  http://localhost:8000/api/change-password/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6' \
  -H 'Content-Type: application/json' \
  -d '{
	"old_password": "admin",
	"new_password": "superadmin"
}'
```

Retorno:

```json
{
    "message": "Password changed!"
}
```

***

#### Department

Endereço: ```http://localhost:8000/api/department/```

Tipo: ```GET``` - List

Exemplo:

```bash
curl -X GET \
  http://localhost:8000/api/department/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6'
```

Retorno:

```json
[
    {
        "id": 1,
        "name": "TI"
    },
    {
        "id": 2,
        "name": "RH"
    },
    {
        "id": 3,
        "name": "ADM"
    }
]
```

***

Endereço: ```http://localhost:8000/api/department/1/```

Tipo: ```GET``` - Detail

Exemplo:

```bash
curl -X GET \
  http://localhost:8000/api/department/1/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6'
```

Retorno:

```json
[
    {
        "id": 1,
        "name": "TI"
    }
]
```

***

Endereço: ```http://localhost:8000/api/department/```

Tipo: ```POST```

Exemplo:

```bash
curl -X POST \
  http://localhost:8000/api/department/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6' \
  -H 'Content-Type: application/json' \
  -d '{"name": "FIN"}'
```

Retorno:

```json
[
    {
        "id": 1,
        "name": "TI"
    },
    {
        "id": 2,
        "name": "RH"
    },
    {
        "id": 3,
        "name": "ADM"
    },
    {
        "id": 4,
        "name": "FIN"
    }
]
```

***

Endereço: ```http://localhost:8000/api/department/4/```

Tipo: ```DELETE```

Exemplo: 

```bash
curl -X DELETE \
  http://localhost:8000/api/department/4/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6'
```

Retorno:

```json
[
    {
        "id": 1,
        "name": "TI"
    },
    {
        "id": 2,
        "name": "RH"
    },
    {
        "id": 3,
        "name": "ADM"
    }
]
```

***

#### User Profile

Endereço: ```http://localhost:8000/api/user-profile/```

Tipo: ```GET``` - List

Exemplo:

```bash
curl -X GET \
  http://localhost:8000/api/user-profile/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6'
```

Retorno:

```json
[
    {
        "profile_id": 1,
        "full_name": "Admin Adminson"
    },
    {
        "profile_id": 2,
        "full_name": "Test Testson"
    },
    {
        "profile_id": 3,
        "full_name": "Fulano Silva"
    }
]
```

***

Endereço: ```http://localhost:8000/api/user-profile/2/```

Tipo: ```GET``` - Details

Exemplo:

```bash
curl -X GET \
  http://localhost:8000/api/user-profile/2/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6'
```

Retorno:

```json
[
    {
        "profile_id": 2,
        "username": "test",
        "email": "test@test.com",
        "full_name": "Test Testson",
        "is_staff": false,
        "department": {
            "id": 1,
            "name": "TI"
        }
    }
]
```

***

Endereço: ```http://localhost:8000/api/user-profile/2/```

Tipo: ```PUT```

Exemplo:

```bash
curl -X PUT \
  http://localhost:8000/api/user-profile/2/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6' \
  -d '{
	"email": "new_test@test.com",
	"full_name": "Newtest Testson",
	"department": "RH"
}'
```

Retorno:

```json
{
    "message": "User profile updated!"
}
```

***

Endereço: ```http://localhost:8000/api/user-profile/2/```

Tipo: ```DELETE```

Exemplo:

```bash
curl -X DELETE \
  http://localhost:8000/api/user-profile/2/ \
  -H 'Authorization: Token 7ee25910ac2842f0963aea046b44a314ec3e10a6'
```

Retorno:

```json
{
    "message": "User profile deleted!"
}
```